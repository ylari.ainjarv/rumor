Kirjutada kuulujutugeneraator.
Kolm ühepikkust hulka: Esimeses naisenimed, teises mehenimed, kolmandas tegusõnad.
Programm võtab igast hulgast ÜHE suvalise elemendi ja kombineerib nendest lause.

Vihje: hulga suvalise elemendi valimiseks arvuta juhuslik number (java.util.Random)
vahemikus 0 kuni hulga pikkus.
